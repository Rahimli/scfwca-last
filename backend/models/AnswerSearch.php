<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Answer;

/**
 * AnswerSearch represents the model behind the search form about `backend\models\Answer`.
 */
class AnswerSearch extends Answer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'active'], 'integer'],
            [['title_az', 'question_id', 'title_en', 'title_ru', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Answer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query -> joinWith('question');
        $query->andFilterWhere([
            'id' => $this->id,
            'question_id' => $this->question_id,
            'active' => $this->active,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'answer.title_az', $this->title_az])
            ->andFilterWhere(['like', 'answer.title_en', $this->title_en])
            ->andFilterWhere(['like', 'answer.title_ru', $this->title_ru])
            ->andFilterWhere(['like', 'question.title_az', $this->question_id]);

        return $dataProvider;
    }
}
