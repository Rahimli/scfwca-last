<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%post_menu}}".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $menu_id
 *
 * @property Menu $menu
 * @property Post $post
 */
class PostMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post_menu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'menu_id'], 'required'],
            [['post_id', 'menu_id'], 'integer'],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'post_id' => Yii::t('backend', 'Post ID'),
            'menu_id' => Yii::t('backend', 'Menu ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosta()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id','active' => 1])->orderBy(['date' => SORT_DESC]);
    }
}
