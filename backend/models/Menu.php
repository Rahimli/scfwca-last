<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property integer $parent_menu_id
 * @property string $name
 * @property string $name_az
 * @property string $name_en
 * @property string $name_ru
 * @property integer $order_index
 * @property string $page_type
 * @property string $position
 * @property integer $active
 * @property string $context_az
 * @property string $context_en
 * @property string $context_ru
 * @property string $slug
 * @property string $date
 * @property string $fa_icon
 * @property string $image
 * @property string $link
 *
 * @property Menu $parentMenu
 * @property Menu[] $menus
 * @property Post[] $posts
 * @property PostView[] $postViews
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_menu_id', 'order_index', 'active'], 'integer'],
            [['name', 'name_az', 'name_en', 'name_ru', 'order_index', 'page_type', 'position', 'slug', 'date'], 'required'],
            [['context_az', 'context_en', 'context_ru'], 'string'],
            [['date'], 'safe'],
            [['name', 'name_az', 'name_en', 'name_ru', 'page_type', 'position', 'slug', 'fa_icon'], 'string', 'max' => 255],
            [['image', 'link'], 'string', 'max' => 500],
            [['parent_menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['parent_menu_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'parent_menu_id' => Yii::t('backend', 'Parent Menu ID'),
            'name' => Yii::t('backend', 'Name'),
            'name_az' => Yii::t('backend', 'Name Az'),
            'name_en' => Yii::t('backend', 'Name En'),
            'name_ru' => Yii::t('backend', 'Name Ru'),
            'order_index' => Yii::t('backend', 'Order Index'),
            'page_type' => Yii::t('backend', 'Page Type'),
            'position' => Yii::t('backend', 'Position'),
            'active' => Yii::t('backend', 'Active'),
            'context_az' => Yii::t('backend', 'Context Az'),
            'context_en' => Yii::t('backend', 'Context En'),
            'context_ru' => Yii::t('backend', 'Context Ru'),
            'slug' => Yii::t('backend', 'Slug'),
            'date' => Yii::t('backend', 'Date'),
            'fa_icon' => Yii::t('backend', 'Fa Icon'),
            'image' => Yii::t('backend', 'Image'),
            'link' => Yii::t('backend', 'Link'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'parent_menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['parent_menu_id' => 'id'])->orderBy('order_index');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getMenuByID($menu_id)
    {
        $menu = Menu::find()->where(['id' => $menu_id])->one();
//        if (!$menu) {
//            $menu = new Menu();
//            $menu->id = $id;
//            $menu->save(false);
//        }
        return $menu;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostMenus()
    {
        return $this->hasMany(PostMenu::className(), ['menu_id' => 'id']);
    }
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['menu' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostViews()
    {
        return $this->hasMany(PostView::className(), ['menu_id' => 'id']);
    }
}
