<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $comment_id
 * @property integer $post_id
 * @property string $first_name
 * @property string $last_name
 * @property string $message
 * @property string $ip
 * @property integer $show
 * @property string $date
 *
 * @property Post $post
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id','comment_id', 'post_id', 'show'], 'integer'],
            [['first_name', 'last_name', 'message'], 'required'],
            [['message'], 'string'],
            [['date'], 'safe'],
            [['first_name', 'last_name'], 'string', 'max' => 100],
            [['ip'], 'string', 'max' => 255],
            [['comment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Comment::className(), 'targetAttribute' => ['comment_id' => 'id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'comment_id' => Yii::t('backend', 'Comment ID'),
            'post_id' => Yii::t('backend', 'Post ID'),
            'first_name' => Yii::t('backend', 'First Name'),
            'last_name' => Yii::t('backend', 'Last Name'),
            'message' => Yii::t('backend', 'Message'),
            'ip' => Yii::t('backend', 'Ip'),
            'show' => Yii::t('backend', 'Show'),
            'date' => Yii::t('backend', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['comment_id' => 'id']);
    }
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}
