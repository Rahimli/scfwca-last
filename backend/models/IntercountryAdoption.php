<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%intercountry_adoption}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $patronyc_name
 * @property string $date
 * @property string $place_of_birth
 * @property string $citizenship
 * @property string $registration
 * @property string $address
 * @property string $home_phone
 * @property string $work_phone
 * @property string $mobile_phone
 * @property string $email
 * @property string $document
 * @property string $sex
 * @property string $education
 * @property string $marital_status
 * @property string $special_features
 * @property string $motivation_to_adopt
 * @property string $apply_date
 */
class IntercountryAdoption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $document_file;
    public static function tableName()
    {
        return '{{%intercountry_adoption}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'patronyc_name', 'date', 'place_of_birth', 'citizenship', 'registration', 'address', 'home_phone', 'work_phone', 'mobile_phone', 'email', 'sex', 'education', 'marital_status', 'special_features', 'motivation_to_adopt'], 'required'],
            [['date', 'apply_date'], 'safe'],
            ['date', 'date', 'format' => 'yyyy-mm-dd'],
            [['special_features', 'motivation_to_adopt'], 'string'],
            [['first_name', 'last_name', 'patronyc_name', 'place_of_birth', 'citizenship', 'registration', 'address', 'home_phone', 'work_phone', 'mobile_phone', 'email', 'education', 'marital_status'], 'string', 'max' => 255],
            [['first_name', 'last_name', 'patronyc_name', 'citizenship', 'registration', 'address', 'education', 'marital_status'], 'string', 'min' => 3],
            [['home_phone', 'work_phone', 'mobile_phone'], 'string', 'min' => 6],
            [['document'], 'string'],
            [['document'], 'file', 'extensions' => 'pdf, doc, docx, txt, png, jpg,zip,rar,tar'],
//            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, bmp ', 'maxFiles' => 100],
            [['sex'], 'string', 'max' => 100, 'min' => 4],
            ['email', 'email'],
        ];
    }
    public function upload()
    {
        if ($this->validate()) {
//            foreach ($this->imageFiles as $file) {
//                $file->saveAs(Url::to('@frontend/web/post-image/') . $file->baseName . '.' . $file->extension);
//            }
            return true;
        } else {
            return false;
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'first_name' => Yii::t('backend', 'First Name'),
            'last_name' => Yii::t('backend', 'Last Name'),
            'patronyc_name' => Yii::t('backend', 'Patronyc Name'),
            'date' => Yii::t('backend', 'Date'),
            'place_of_birth' => Yii::t('backend', 'Place Of Birth'),
            'citizenship' => Yii::t('backend', 'Citizenship'),
            'registration' => Yii::t('backend', 'Registration'),
            'address' => Yii::t('backend', 'Address'),
            'home_phone' => Yii::t('backend', 'Home Phone'),
            'work_phone' => Yii::t('backend', 'Work Phone'),
            'mobile_phone' => Yii::t('backend', 'Mobile Phone'),
            'email' => Yii::t('backend', 'Email'),
            'document' => Yii::t('backend', 'Document'),
            'sex' => Yii::t('backend', 'Sex'),
            'education' => Yii::t('backend', 'Education'),
            'marital_status' => Yii::t('backend', 'Marital Status'),
            'special_features' => Yii::t('backend', 'Special Features'),
            'motivation_to_adopt' => Yii::t('backend', 'Motivation To Adopt'),
            'apply_date' => Yii::t('backend', 'Apply Date'),
        ];
    }
}
