<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%suggestion}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property string $suggest
 * @property string $date
 */
class Suggestion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%suggestion}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'phone', 'email', 'suggest'], 'required'],
            [['suggest'], 'string'],
            ['email', 'email'],
            [['date'], 'safe'],
            [['first_name', 'last_name', 'phone', 'email'], 'string', 'max' => 255],
            [['first_name', 'last_name'], 'string', 'min' => 3],
            [['phone'], 'string', 'min' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'first_name' => Yii::t('backend', 'First Name'),
            'last_name' => Yii::t('backend', 'Last Name'),
            'phone' => Yii::t('backend', 'Phone'),
            'email' => Yii::t('backend', 'Email'),
            'suggest' => Yii::t('backend', 'Suggest'),
            'date' => Yii::t('backend', 'Date'),
        ];
    }
}
