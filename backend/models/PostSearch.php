<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Post;

/**
 * PostSearch represents the model behind the search form about `backend\models\Post`.
 */
class PostSearch extends Post
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'show_homepage', 'active', 'read_count'], 'integer'],
            [['name', 'name_az', 'name_en', 'name_ru', 'image', 'short_text_az', 'short_text_en', 'short_text_ru', 'context_az', 'context_en', 'context_ru', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query -> joinWith('menu0');
        $query->andFilterWhere([
            'id' => $this->id,
            'show_homepage' => $this->show_homepage,
            'active' => $this->active,
            'read_count' => $this->read_count,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'post.name', $this->name])
            ->andFilterWhere(['like', 'post.name_az', $this->name_az])
            ->andFilterWhere(['like', 'post.name_en', $this->name_en])
            ->andFilterWhere(['like', 'post.name_ru', $this->name_ru])
            ->andFilterWhere(['like', 'image', $this->image])
//            ->andFilterWhere(['like', 'menu.name', $this->menu])
//            ->andFilterWhere(['like', 'menu.name_az', $this->menu])
//            ->andFilterWhere(['like', 'menu.name_en', $this->menu])
//            ->andFilterWhere(['like', 'menu.name_ru', $this->menu])
            ->andFilterWhere(['like', 'short_text_az', $this->short_text_az])
            ->andFilterWhere(['like', 'short_text_en', $this->short_text_en])
            ->andFilterWhere(['like', 'short_text_ru', $this->short_text_ru])
            ->andFilterWhere(['like', 'context_az', $this->context_az])
            ->andFilterWhere(['like', 'context_en', $this->context_en])
            ->andFilterWhere(['like', 'context_ru', $this->context_ru]);

//        $query -> joinWith('menu0');
//        $query->joinWith(['menu0']);;
//            ->andFilterWhere(['like', 'menu.name_az', $this->name_az])
//            ->andFilterWhere(['like', 'menu.name_en', $this->name_en])
//            ->andFilterWhere(['like', 'menu.name_ru', $this->name_ru])
        return $dataProvider;
    }
}
