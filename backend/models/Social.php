<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "social".
 *
 * @property integer $id
 * @property string $name
 * @property string $icon
 * @property string $link
 * @property integer $order_index
 * @property integer $active
 */
class Social extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'social';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'icon', 'link', 'order_index'], 'required'],
            [['order_index', 'active'], 'integer'],
            [['name', 'icon'], 'string', 'max' => 255],
            [['link'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'icon' => Yii::t('backend', 'Icon'),
            'link' => Yii::t('backend', 'Link'),
            'order_index' => Yii::t('backend', 'Order Index'),
            'active' => Yii::t('backend', 'Active'),
        ];
    }
}
