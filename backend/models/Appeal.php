<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%appeal}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $birth_date
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property string $context
 * @property string $file
 * @property string $date
 */
class Appeal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $document_file;
//    public $verificationCode;
    public static function tableName()
    {
        return '{{%appeal}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'birth_date', 'phone', 'email', 'address', 'context'], 'required'],
            [['birth_date', 'date'], 'safe'],
            ['birth_date', 'date', 'format' => 'yyyy-mm-dd'],
            [['context'], 'string', 'min' => 3],
            [['first_name', 'last_name', 'phone', 'email', 'address'], 'string', 'max' => 255],
            [['first_name', 'last_name', 'phone','address'], 'string', 'min' => 3],
            [['phone'], 'string', 'min' => 7],
            [['file'], 'string', 'max' => 500],
//            [['document'], 'string'],
//            [['verificationCode'], 'captcha'],
            ['email', 'email'],
            [['document_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf, doc, docx, txt, png, jpg,zip,rar,tar'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
//            foreach ($this->imageFiles as $file) {
//                $file->saveAs(Url::to('@frontend/web/post-image/') . $file->baseName . '.' . $file->extension);
//            }
            return true;
        } else {
            return false;
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'first_name' => Yii::t('backend', 'First Name'),
            'last_name' => Yii::t('backend', 'Last Name'),
            'birth_date' => Yii::t('backend', 'Birth Date'),
            'phone' => Yii::t('backend', 'Phone'),
            'email' => Yii::t('backend', 'Email'),
            'address' => Yii::t('backend', 'Address'),
            'context' => Yii::t('backend', 'Context'),
            'file' => Yii::t('backend', 'File'),
            'date' => Yii::t('backend', 'Date'),
        ];
    }
}
