<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\IntercountryAdoption;

/**
 * IntercountryAdoptionSearch represents the model behind the search form about `backend\models\IntercountryAdoption`.
 */
class IntercountryAdoptionSearch extends IntercountryAdoption
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['first_name', 'last_name', 'patronyc_name', 'date', 'place_of_birth', 'citizenship', 'registration', 'address', 'home_phone', 'work_phone', 'mobile_phone', 'email', 'document', 'education', 'sex', 'marital_status', 'special_features', 'motivation_to_adopt', 'apply_date'], 'safe'],
            [['sex'], 'in','range'=>['male','female']],
            [['document'], 'file', 'extensions' => 'pdf, doc, docx, txt, png, jpg,zip,rar,tar']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IntercountryAdoption::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'sex' => $this->sex,
            'apply_date' => $this->apply_date,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'patronyc_name', $this->patronyc_name])
            ->andFilterWhere(['like', 'place_of_birth', $this->place_of_birth])
            ->andFilterWhere(['like', 'citizenship', $this->citizenship])
            ->andFilterWhere(['like', 'registration', $this->registration])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'home_phone', $this->home_phone])
            ->andFilterWhere(['like', 'work_phone', $this->work_phone])
            ->andFilterWhere(['like', 'mobile_phone', $this->mobile_phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'document', $this->document])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'marital_status', $this->marital_status])
            ->andFilterWhere(['like', 'special_features', $this->special_features])
            ->andFilterWhere(['like', 'motivation_to_adopt', $this->motivation_to_adopt]);

        return $dataProvider;
    }
}
