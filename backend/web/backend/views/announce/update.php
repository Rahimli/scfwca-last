<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Announce */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Announce',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Announces'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="announce-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
