<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 3/29/2017
 * Time: 13:30
 */
echo \kato\DropZone::widget([
    'options' => [
        'url' => 'upload',
        'maxFilesize' => '2',
    ],
    'clientEvents' => [
        'complete' => "function(file){console.log(file)}",
        'removedfile' => "function(file){alert(file.name + ' is removed')}"
    ],
]);
?>