<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PostSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name_az') ?>

<!--    --><?//= $form->field($model, 'name_en') ?>
<!---->
<!--    --><?php // echo $form->field($model, 'name_ru') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'show_homepage') ?>

    <?php // echo $form->field($model, 'active') ?>

    <?php // echo $form->field($model, 'short_text_az') ?>

    <?php // echo $form->field($model, 'short_text_en') ?>

    <?php // echo $form->field($model, 'short_text_ru') ?>

    <?php // echo $form->field($model, 'context_az') ?>

    <?php // echo $form->field($model, 'context_en') ?>

    <?php // echo $form->field($model, 'context_ru') ?>

    <?php // echo $form->field($model, 'date') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
