<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\ElFinder;


$get_current_language = Yii::$app->language;

/* @var $this yii\web\View */
///* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .searchable-container {
        margin: 20px 0 0 0
    }

    .searchable-container label.btn-default.active {
        background-color: #007ba7;
        color: #FFF
    }

    .searchable-container label.btn-default {
        width: 90%;
        border: 1px solid #efefef;
        margin: 5px;
        box-shadow: 5px 8px 8px 0 #ccc;
    }

    .searchable-container label .bizcontent {
        width: 100%;
    }

    .searchable-container .btn-group {
        width: 90%
    }

    .searchable-container .btn span.glyphicon {
        opacity: 0;
    }

    .searchable-container .btn.active span.glyphicon {
        opacity: 1;
    }

    .list-group {
        overflow: hidden;
        border-left: 1px solid rgb(221, 221, 221);
        border-right: 1px solid rgb(221, 221, 221);
    }

    .list-group-item:first-child, .list-group-item:last-child {
        border-top-right-radius: 0px;
        border-top-left-radius: 0px;
        border-bottom-right-radius: 0px;
        border-bottom-left-radius: 0px;
        overflow: hidden;
    }

    .list-group-item {
        border-left: 0px solid rgb(221, 221, 221);
        border-right: 0px solid rgb(221, 221, 221);
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
    }

    .list-group-item > .show-menu {
        position: absolute;
        height: 100%;
        width: 24px;
        top: 0px;
        right: 0px;
        background-color: rgba(51, 51, 51, 0.2);
        cursor: pointer;
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
    }

    .list-group-item > .show-menu > span {
        position: absolute;
        top: 50%;
        margin-top: -7px;
        padding: 0px 5px;
    }

    .list-group-submenu {
        position: absolute;
        top: 0px;
        /*right: -88px;*/
        white-space: nowrap;
        list-style: none;
        padding-left: 0px;
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
    }

    .list-group-submenu .list-group-submenu-item {
        float: right;
        white-space: nowrap;
        display: block;
        padding: 10px 15px;
        margin-bottom: -1px;
        background-color: rgb(51, 51, 51);
        color: rgb(235, 235, 235);
    }

    .list-group-item.open {
        /*margin-left: -88px;*/
    }

    .list-group-item.open > .show-menu {
        right: 44px;
    }

    .list-group-item.open .list-group-submenu {
        right: 0px;
    }

    .list-group-submenu .list-group-submenu-item.primary {
        color: rgb(255, 255, 255);
        background-color: rgb(50, 118, 177);
    }

    .list-group-submenu .list-group-submenu-item.success {
        color: rgb(255, 255, 255);
        background-color: rgb(92, 184, 92);
    }

    .list-group-submenu .list-group-submenu-item.info {
        color: rgb(255, 255, 255);
        background-color: rgb(57, 179, 215);
    }

    .list-group-submenu .list-group-submenu-item.warning {
        color: rgb(255, 255, 255);
        background-color: rgb(240, 173, 78);
    }

    .list-group-submenu .list-group-submenu-item.danger {
        color: rgb(255, 255, 255);
        background-color: rgb(217, 83, 79);
    }
</style>
<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>



    <?php $menuArray = ArrayHelper::map(\backend\models\Menu::find()->where(['page_type' => 'cp'])->orderBy('name_'.$get_current_language)->all(), 'id', 'name_'.$get_current_language) ?>


    <?php echo $form->field($model, 'menu_ids')->widget(Select2::classname(), [
        'data' => $menuArray,
        'options' => ['multiple' => true,'placeholder' => '-- Choose Menu --'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Category');
    ?>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_az')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'show_homepage')->checkbox() ?>

    <?= $form->field($model, 'show_comment')->checkbox() ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true])->widget(InputFile::className(), [
        'language' => 'en',
        'controller' => 'fm',
        'path' => '/post',
        'filter' => 'image',
        'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options' => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-default'],
    ]) ?>

    <div class="row" style="margin-bottom: 10px;">
        <div class="form-group">
            <div class="searchable-container">
                <div id="lang-div-az"
                     onclick="if($('.field-post-context_az').is(':visible')){$('.field-post-context_az').hide();$('.field-post-short_text_az').hide();}else{$('.field-post-context_az').show();$('.field-post-short_text_az').show();}"
                     class="items col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="info-block block-info clearfix" style="width: 100%;">
                        <div data-toggle="buttons" class="btn-group bizmoduleselect" style="width: 100%;">
                            <label class="btn btn-default" style="width: 100%;">
                                <div class="bizcontent" style="width: 100%;">
                                    <input type="checkbox" name="var_id[]" autocomplete="off" value="">
                                    <!--                                    <span class="glyphicon glyphicon-ok glyphicon-lg"></span>-->
                                    <h5><img height="30" src="/addmin_static/img/az.gif"></h5>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
                <div id="lang-div-eng"
                     onclick="if($('.field-post-context_en').is(':visible')){$('.field-post-context_en').hide();$('.field-post-short_text_en').hide();}else{$('.field-post-context_en').show();$('.field-post-short_text_en').show();}"
                     class="items col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="info-block block-info clearfix" style="width: 100%;">
                        <div data-toggle="buttons" class="btn-group bizmoduleselect" style="width: 100%;">
                            <label class="btn btn-default" style="width: 100%;">
                                <div class="bizcontent">
                                    <input type="checkbox" name="var_id[]" autocomplete="off" value="">
                                    <!--                                    <span class="glyphicon glyphicon-ok glyphicon-lg"></span>-->
                                    <h5><img height="30"
                                             src="/addmin_static/img/1280px-Flag_of_the_United_Kingdom.svg.png">
                                    </h5>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
                <div id="lang-div-ru"
                     onclick="if($('.field-post-context_ru').is(':visible')){$('.field-post-context_ru').hide();$('.field-post-short_text_ru').hide();}else{$('.field-post-context_ru').show();$('.field-post-short_text_ru').show();}"
                     class="items col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="info-block block-info clearfix" style="width: 100%;">
                        <div data-toggle="buttons" class="btn-group bizmoduleselect" style="width: 100%;">
                            <label class="btn btn-default" style="width: 100%;">
                                <div class="bizcontent">
                                    <input type="checkbox" name="var_id[]" autocomplete="off" value="">
                                    <!--                                    <span class="glyphicon glyphicon-ok glyphicon-lg"></span>-->
                                    <h5><img height="30"
                                             src="/addmin_static/img/20120812153730!Flag_of_Russia.svg">
                                    </h5>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'short_text_az')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_text_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_text_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'context_az')->widget(CKEditor::className(),
        ['editorOptions' => ElFinder::ckeditorOptions('fm',
            ['preset' => 'full',
                'language' => 'en',
//                'controller' => 'fm',
                'path' => '/announce',
                'filter' => 'image',

            ]),
        ]);; ?>

    <?= $form->field($model, 'context_en')->widget(CKEditor::className(),
        ['editorOptions' => ElFinder::ckeditorOptions('fm',
            ['preset' => 'full',
                'language' => 'en',
//                'controller' => 'fm',
                'path' => '/announce',
                'filter' => 'image',

            ]),
        ]);; ?>

    <?= $form->field($model, 'context_ru')->widget(CKEditor::className(),
        ['editorOptions' => ElFinder::ckeditorOptions('fm',
            ['preset' => 'full',
                'language' => 'en',
//                'controller' => 'fm',
                'path' => '/announce',
                'filter' => 'image',
                'multiple' => true,
            ]),
        ]); ?>




    <?= $form->field($model, 'active')->checkbox() ?>

    <?= $form->field($model, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?php
    echo DatePicker::widget([
        'model' => $model,
        'attribute' => 'date',
//    'options' => ['placeholder' => 'Enter birth date ...'],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>
    </br>
    <?php
    echo DatePicker::widget([
        'model' => $model,
        'attribute' => 'end_date',
//    'options' => ['placeholder' => 'Enter birth date ...'],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>
    </br>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </br>
    <?php if (!$model->isNewRecord) { ?>
        <?php if (!empty($model->pimages)) { ?>
            <div class="container">
                <div class="row">
                    <h2>Slider Images</h2>
                </div>
                <div class="row">
                    <div class="col-sm-10">
                        <ul class="list-group">

                            <?php foreach ($model->pimages as $image) { ?>
                                <li id="model-image-post-id-<?= $image->id; ?>" class="list-group-item open">
                                    <img height="100" src="<?= $image->image_url; ?>">
                                    <span id="model-post-span-id-<?= $image->id; ?>" class="show-menu">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </span>
                                    <ul class="list-group-submenu">
                                        <li onclick="deleteImage(<?= $image->id; ?>,'<?php echo Url::toRoute(['post/deleteimages', 'id' => $model->id]) ?>')"
                                            class="list-group-submenu-item danger"><span
                                                    class="glyphicon glyphicon-remove danger"></span></li>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php } ?>

    <?php } ?>


    <br>
    <script src="/addmin_static/js/jquery.min.js"></script>
    <link rel="stylesheet" href="/addmin_static/css/jquery-confirm.min.css">
    <script src="/addmin_static/js/jquery-confirm.min.js"></script>
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
<!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.css">-->
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>-->
    <script>
        $(function () {
            $('.list-group-item > .show-menu').on('click', function (event) {
                event.preventDefault();
                $(this).closest('li').toggleClass('open');
            });
        });
    </script>

    <script>
        //    var online_survey_form = $("#online-survey-form");
        //    var vote_submit_button = $("#vote-submit-button");
        function deleteImage($id, $url) {


            $.ajax({
                url: $url, // the endpoint,commonly same url
                dataType: "json",
                type: "GET",
                data: {
                    image_id: $id
                },
                success: function (json) {

                    if (json['message_code'] == 1) {

                        $('#model-image-post-id-' + $id).hide();

                    } else {
                        alert(json['message_code']);
                    }
                },
                // handle a non-successful response
                error: function (xhr, errmsg, err) {
//                vote_submit_button.show();
//                    location.reload();
                    alert('error');
                    // contact_fullname.removeClass("invalid valid");
                    // contact_email.removeClass("invalid valid");
                    // contact_message.removeClass("invalid valid");
                    // contact_loader.hide();
                    // contact_invalid_text.show();
                    // setTimeout(function () {
                    //     contact_invalid_text.fadeOut();
                    //     contactButton.show(1000);
                    // }, 4000);
                    // contact_invalid_text.append('<strong>Error ! Please, try again </strong>');
                }
            });
        }

    </script>

    <script>
        document.getElementById('post-image').setAttribute("readonly", true);
    </script>
    <script>
        var lang_first_div = $('#lang-first-div');
        var post_context_az = $('.field-post-context_az');
        var post_context_en = $('.field-post-context_en');
        var post_context_ru = $('.field-post-context_ru');
        var post_short_text_az = $('.field-post-short_text_az');
        var post_short_text_en = $('.field-post-short_text_en');
        var post_short_text_ru = $('.field-post-short_text_ru');
        post_context_az.hide();
        post_context_en.hide();
        post_context_ru.hide();
        post_short_text_az.hide();
        post_short_text_en.hide();
        post_short_text_ru.hide();
    </script>
    <script>
        //    * @param string s
        //    * @param object opt
        //    * @return string

        function url_slug(s, opt) {
            s = String(s);
            opt = Object(opt);

            var defaults = {
                'delimiter': '-',
                'limit': 100,
                'lowercase': true,
                'replacements': {},
                'transliterate': (typeof(XRegExp) === 'undefined') ? true : false
            };

            // Merge options
            for (var k in defaults) {
                if (!opt.hasOwnProperty(k)) {
                    opt[k] = defaults[k];
                }
            }

            var char_map = {
                // Latin
                'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE', 'Ç': 'C',
                'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I', 'Î': 'I', 'Ï': 'I',
                'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O', 'Õ': 'O', 'Ö': 'O', 'Ő': 'O',
                'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U', 'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH',
                'ß': 'ss',
                'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c',
                'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',
                'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ő': 'o',
                'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ű': 'u', 'ý': 'y', 'þ': 'th',
                'ÿ': 'y',

                // Latin symbols
                '©': '(c)',

                // Greek
                'Α': 'A', 'Β': 'B', 'Γ': 'G', 'Δ': 'D', 'Ε': 'E', 'Ζ': 'Z', 'Η': 'H', 'Θ': '8',
                'Ι': 'I', 'Κ': 'K', 'Λ': 'L', 'Μ': 'M', 'Ν': 'N', 'Ξ': '3', 'Ο': 'O', 'Π': 'P',
                'Ρ': 'R', 'Σ': 'S', 'Τ': 'T', 'Υ': 'Y', 'Φ': 'F', 'Χ': 'X', 'Ψ': 'PS', 'Ω': 'W',
                'Ά': 'A', 'Έ': 'E', 'Ί': 'I', 'Ό': 'O', 'Ύ': 'Y', 'Ή': 'H', 'Ώ': 'W', 'Ϊ': 'I',
                'Ϋ': 'Y',
                'α': 'a', 'β': 'b', 'γ': 'g', 'δ': 'd', 'ε': 'e', 'ζ': 'z', 'η': 'h', 'θ': '8',
                'ι': 'i', 'κ': 'k', 'λ': 'l', 'μ': 'm', 'ν': 'n', 'ξ': '3', 'ο': 'o', 'π': 'p',
                'ρ': 'r', 'σ': 's', 'τ': 't', 'υ': 'y', 'φ': 'f', 'χ': 'x', 'ψ': 'ps', 'ω': 'w',
                'ά': 'a', 'έ': 'e', 'ί': 'i', 'ό': 'o', 'ύ': 'y', 'ή': 'h', 'ώ': 'w', 'ς': 's',
                'ϊ': 'i', 'ΰ': 'y', 'ϋ': 'y', 'ΐ': 'i',

                // Turkish
                'Ş': 'S', 'İ': 'I', 'Ç': 'C', 'Ü': 'U', 'Ö': 'O', 'Ğ': 'G',
                'ş': 's', 'ı': 'i', 'ç': 'c', 'ü': 'u', 'ö': 'o', 'ğ': 'g',

                // Russian
                'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
                'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
                'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
                'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
                'Я': 'Ya',
                'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
                'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
                'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
                'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
                'я': 'ya',

                // Ukrainian
                'Є': 'Ye', 'І': 'I', 'Ї': 'Yi', 'Ґ': 'G',
                'є': 'ye', 'і': 'i', 'ї': 'yi', 'ґ': 'g',

                // Azerbaijanian
                'Ə': 'E',
                'ə': 'e',

                // Czech
                'Č': 'C', 'Ď': 'D', 'Ě': 'E', 'Ň': 'N', 'Ř': 'R', 'Š': 'S', 'Ť': 'T', 'Ů': 'U',
                'Ž': 'Z',
                'č': 'c', 'ď': 'd', 'ě': 'e', 'ň': 'n', 'ř': 'r', 'š': 's', 'ť': 't', 'ů': 'u',
                'ž': 'z',

                // Polish
                'Ą': 'A', 'Ć': 'C', 'Ę': 'e', 'Ł': 'L', 'Ń': 'N', 'Ó': 'o', 'Ś': 'S', 'Ź': 'Z',
                'Ż': 'Z',
                'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ó': 'o', 'ś': 's', 'ź': 'z',
                'ż': 'z',

                // Latvian
                'Ā': 'A', 'Č': 'C', 'Ē': 'E', 'Ģ': 'G', 'Ī': 'i', 'Ķ': 'k', 'Ļ': 'L', 'Ņ': 'N',
                'Š': 'S', 'Ū': 'u', 'Ž': 'Z',
                'ā': 'a', 'č': 'c', 'ē': 'e', 'ģ': 'g', 'ī': 'i', 'ķ': 'k', 'ļ': 'l', 'ņ': 'n',
                'š': 's', 'ū': 'u', 'ž': 'z'
            };

            // Make custom replacements
            for (var k in opt.replacements) {
                s = s.replace(RegExp(k, 'g'), opt.replacements[k]);
            }

            // Transliterate characters to ASCII
            if (opt.transliterate) {
                for (var k in char_map) {
                    s = s.replace(RegExp(k, 'g'), char_map[k]);
                }
            }

            // Replace non-alphanumeric characters with our delimiter
            var alnum = (typeof(XRegExp) === 'undefined') ? RegExp('[^a-z0-9]+', 'ig') : XRegExp('[^\\p{L}\\p{N}]+', 'ig');
            s = s.replace(alnum, opt.delimiter);

            // Remove duplicate delimiters
            s = s.replace(RegExp('[' + opt.delimiter + ']{2,}', 'g'), opt.delimiter);

            // Truncate slug to max. characters
            s = s.substring(0, opt.limit);

            // Remove delimiter from ends
            s = s.replace(RegExp('(^' + opt.delimiter + '|' + opt.delimiter + '$)', 'g'), '');

            return opt.lowercase ? s.toLowerCase() : s;
        }

    </script>
    <script>
        var post_name = $("#post-name");
        var post_slug_d = $("#post-slug");
        post_name.keyup(function () {
            str = post_name.val();
            post_slug_d.val(url_slug(str));
//        }
        });

    </script>

    <?php ActiveForm::end(); ?>

</div>
