<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Post */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
//            ['attribute'=>'menu',
//                'value' => $model->menu0->name_az,],
//            'menu',
            'name',
            'name_az',
            'name_en',
            'name_ru',
            'show_homepage',
            'image',
            'read_count',
            'short_text_az',
            'short_text_en',
            'short_text_ru',
            'context_az:ntext',
            'context_en:ntext',
            'context_ru:ntext',
            'active',
            'slug',
            'end_date',
            'date',
        ],
    ]) ?>

</div>
