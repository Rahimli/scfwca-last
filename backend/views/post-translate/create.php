<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PostTranslate */

$this->title = Yii::t('backend', 'Create Post Translate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Post Translates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-translate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
