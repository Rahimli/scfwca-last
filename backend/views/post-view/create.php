<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PostView */

$this->title = Yii::t('backend', 'Create Post View');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Post Views'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
