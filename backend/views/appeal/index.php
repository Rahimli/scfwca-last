<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AppealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Appeals');
$this->params['breadcrumbs'][] = $this->title;
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['<?= Yii::t('backend', 'Year'); ?>', '<?= Yii::t('backend', 'Appeal'); ?>'],
            <?php foreach ($data_years as $key => $data_year){?>
            [<?= $data_year['year']?>,  <?= $data_year['count']?>],
            <?php }?>
        ]);

        var options = {
            title: '<?= Yii::t('backend', 'Appeals'); ?>',
            curveType: 'function',
            legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
    }
</script>
<div class="appeal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'first_name',
            'last_name',
            'birth_date',
            'phone',
            // 'email:email',
            // 'address',
            // 'context:ntext',
            // 'file',
            // 'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<hr>

<div id="curve_chart" style="width: 100%; height: 500px"></div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<script>
    $("tbody tr td").each(function() {
        var htmlRegex = new RegExp("<([A-Za-z][A-Za-z0-9]*)\b[^>]*>(.*?)</\1>");
        if($(this).html().indexOf("<a") < 0){
            // Within tr we find the last td child element and get content
            $(this).text($(this).text().slice(0, 25));
        }
    });
</script>