<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Suggestion */

$this->title = Yii::t('backend', 'Create Suggestion');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Suggestions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suggestion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
