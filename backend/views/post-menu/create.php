<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PostMenu */

$this->title = Yii::t('backend', 'Create Post Menu');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Post Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-menu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
