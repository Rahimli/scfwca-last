<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\QuestionResult */

$this->title = Yii::t('backend', 'Create Question Result');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Question Results'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-result-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
