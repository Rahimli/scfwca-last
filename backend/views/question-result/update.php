<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\QuestionResult */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Question Result',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Question Results'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="question-result-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
