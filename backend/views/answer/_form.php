<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$get_current_language = Yii::$app->language;

/* @var $this yii\web\View */
/* @var $model backend\models\Answer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="answer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $questionArray = ArrayHelper::map(\backend\models\Question::find()->orderBy('title_'.$get_current_language)->all(), 'id', 'title_'.$get_current_language) ?>

    <?php echo $form->field($model, 'question_id')->widget(Select2::classname(), [
        'data' => $questionArray,
        'options' => ['placeholder' => '-- Choose Question --'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('İmtahan');
    ?>
    <?= $form->field($model, 'title_az')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'active')->checkbox()?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
