<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\IntercountryAdoption */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Intercountry Adoptions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intercountry-adoption-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'patronyc_name',
            'date',
            'place_of_birth',
            'citizenship',
            'registration',
            'address',
            'home_phone',
            'work_phone',
            'mobile_phone',
            'email:email',
            'document',
            [
                'attribute'=>'document',
                'format'=>'raw',
                'value' => function($data)
                {
                    return
                        Html::a('Open file', ''.$data->document.'', ['class' => 'btn btn-primary',]);

                }
            ],
            'sex',
            'education',
            'marital_status',
            'special_features:ntext',
            'motivation_to_adopt:ntext',
            'apply_date',
        ],
    ]) ?>

</div>
