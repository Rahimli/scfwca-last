<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\ElFinder;


$get_current_language = Yii::$app->language;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .searchable-container {
        margin: 20px 0 0 0
    }

    .searchable-container label.btn-default.active {
        background-color: #007ba7;
        color: #FFF
    }

    .searchable-container label.btn-default {
        width: 90%;
        border: 1px solid #efefef;
        margin: 5px;
        box-shadow: 5px 8px 8px 0 #ccc;
    }

    .searchable-container label .bizcontent {
        width: 100%;
    }

    .searchable-container .btn-group {
        width: 90%
    }

    .searchable-container .btn span.glyphicon {
        opacity: 0;
    }

    .searchable-container .btn.active span.glyphicon {
        opacity: 1;
    }

</style>
<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $menuArray = ArrayHelper::map(\backend\models\Menu::find()->orderBy('name_'.$get_current_language)->all(), 'id', 'name_'.$get_current_language) ?>


    <?php echo $form->field($model, 'parent_menu_id')->widget(Select2::classname(), [
        'data' => $menuArray,
        'options' => ['placeholder' => '-- Choose Menu --'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Parent Menu');
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_az')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_index')->textInput() ?>

    <?= $form->field($model, 'position')->dropDownList(['' => 'Position', 'header' => 'Header', 'slider-under' => 'Under Slider', 'right-forum-top' => 'Right Forum Top', 'right-menu-forum' => 'Right Menu Forum','left-menu-1' => 'Right Menu 1', 'left-menu-2' => 'Right Menu 2', 'right-menu-1' => 'Right Menu 3', 'right-menu-under-video' => 'Right Menu 4','left-menu-3' => 'Right Menu 5', 'left-menu-4' => 'Right Menu 6', 'other' => 'Other', 'home' => 'Home']); ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true])->widget(InputFile::className(), [
        'language' => 'en',
        'controller' => 'fm',
        'path' => '/menu',
        'filter' => 'image',
        'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options' => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-default'],
    ]) ?>
    <?= $form->field($model, 'fa_icon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'page_type')->dropDownList(['' => 'Page Type', 'sp' => 'Static Page', 'cp' => 'Category Page', 'lp' => 'Link']); ?>
    <div id="lang-first-div" style="display: none;">
        <div class="row" style="margin-bottom: 10px;">
            <div class="form-group">
                <div class="searchable-container">
                    <div id="lang-div-az" class="items col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="info-block block-info clearfix" style="width: 100%;">
                            <div data-toggle="buttons" class="btn-group bizmoduleselect" style="width: 100%;">
                                <label class="btn btn-default" style="width: 100%;">
                                    <div class="bizcontent" style="width: 100%;">
                                        <input type="checkbox" name="var_id[]" autocomplete="off" value="">
                                        <!--                                    <span class="glyphicon glyphicon-ok glyphicon-lg"></span>-->
                                        <h5><img height="30" src="/addmin_static/img/az.gif"></h5>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div id="lang-div-eng"
                         onclick="if($('.field-menu-context_en').is(':visible')){$('.field-menu-context_en').hide();}else{$('.field-menu-context_en').show()}"
                         class="items col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="info-block block-info clearfix" style="width: 100%;">
                            <div data-toggle="buttons" class="btn-group bizmoduleselect" style="width: 100%;">
                                <label class="btn btn-default" style="width: 100%;">
                                    <div class="bizcontent">
                                        <input type="checkbox" name="var_id[]" autocomplete="off" value="">
                                        <!--                                    <span class="glyphicon glyphicon-ok glyphicon-lg"></span>-->
                                        <h5><img height="30"
                                                 src="/addmin_static/img/1280px-Flag_of_the_United_Kingdom.svg.png">
                                        </h5>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div id="lang-div-ru"
                         onclick="if($('.field-menu-context_ru').is(':visible')){$('.field-menu-context_ru').hide()}else{$('.field-menu-context_ru').show()}"
                         class="items col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="info-block block-info clearfix" style="width: 100%;">
                            <div data-toggle="buttons" class="btn-group bizmoduleselect" style="width: 100%;">
                                <label class="btn btn-default" style="width: 100%;">
                                    <div class="bizcontent">
                                        <input type="checkbox" name="var_id[]" autocomplete="off" value="">
                                        <!--                                    <span class="glyphicon glyphicon-ok glyphicon-lg"></span>-->
                                        <h5><img height="30"
                                                 src="/addmin_static/img/20120812153730!Flag_of_Russia.svg">
                                        </h5>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?= $form->field($model, 'context_az')->widget(CKEditor::className(),
            ['editorOptions' => ElFinder::ckeditorOptions('fm',
                ['preset' => 'full',
                    'language' => 'az',
//                'controller' => 'fm',
                    'path' => '/menu',
//                    'filter' => 'image',

                ]),
            ]); ?>
        <?= $form->field($model, 'context_en')->widget(CKEditor::className(),
            ['editorOptions' => ElFinder::ckeditorOptions('fm',
                ['preset' => 'full',
                    'language' => 'en',
//                'controller' => 'fm',
                    'path' => '/menu',
//                    'filter' => 'image',

                ]),
            ]); ?>
        <?= $form->field($model, 'context_ru')->widget(CKEditor::className(),
            ['editorOptions' => ElFinder::ckeditorOptions('fm',
                ['preset' => 'full',
                    'language' => 'ru',
//                'controller' => 'fm',
                    'path' => '/menu',
//                    'filter' => 'image',

                ]),
            ]); ?>
    </div>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'active')->checkbox() ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?php
    echo DatePicker::widget([
        'model' => $model,
        'attribute' => 'date',
//    'options' => ['placeholder' => 'Enter birth date ...'],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="/addmin_static/js/jquery.min.js"></script>

<script>
    //    * @param string s
    //    * @param object opt
    //    * @return string
    document.getElementById('menu-image').setAttribute("readonly", true);

    function url_slug(s, opt) {
        s = String(s);
        opt = Object(opt);

        var defaults = {
            'delimiter': '-',
            'limit': undefined,
            'lowercase': true,
            'replacements': {},
            'transliterate': (typeof(XRegExp) === 'undefined') ? true : false
        };

        // Merge options
        for (var k in defaults) {
            if (!opt.hasOwnProperty(k)) {
                opt[k] = defaults[k];
            }
        }

        var char_map = {
            // Latin
            'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE', 'Ç': 'C',
            'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I', 'Î': 'I', 'Ï': 'I',
            'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O', 'Õ': 'O', 'Ö': 'O', 'Ő': 'O',
            'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U', 'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH',
            'ß': 'ss',
            'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c',
            'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',
            'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ő': 'o',
            'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ű': 'u', 'ý': 'y', 'þ': 'th',
            'ÿ': 'y',

            // Latin symbols
            '©': '(c)',

            // Greek
            'Α': 'A', 'Β': 'B', 'Γ': 'G', 'Δ': 'D', 'Ε': 'E', 'Ζ': 'Z', 'Η': 'H', 'Θ': '8',
            'Ι': 'I', 'Κ': 'K', 'Λ': 'L', 'Μ': 'M', 'Ν': 'N', 'Ξ': '3', 'Ο': 'O', 'Π': 'P',
            'Ρ': 'R', 'Σ': 'S', 'Τ': 'T', 'Υ': 'Y', 'Φ': 'F', 'Χ': 'X', 'Ψ': 'PS', 'Ω': 'W',
            'Ά': 'A', 'Έ': 'E', 'Ί': 'I', 'Ό': 'O', 'Ύ': 'Y', 'Ή': 'H', 'Ώ': 'W', 'Ϊ': 'I',
            'Ϋ': 'Y',
            'α': 'a', 'β': 'b', 'γ': 'g', 'δ': 'd', 'ε': 'e', 'ζ': 'z', 'η': 'h', 'θ': '8',
            'ι': 'i', 'κ': 'k', 'λ': 'l', 'μ': 'm', 'ν': 'n', 'ξ': '3', 'ο': 'o', 'π': 'p',
            'ρ': 'r', 'σ': 's', 'τ': 't', 'υ': 'y', 'φ': 'f', 'χ': 'x', 'ψ': 'ps', 'ω': 'w',
            'ά': 'a', 'έ': 'e', 'ί': 'i', 'ό': 'o', 'ύ': 'y', 'ή': 'h', 'ώ': 'w', 'ς': 's',
            'ϊ': 'i', 'ΰ': 'y', 'ϋ': 'y', 'ΐ': 'i',

            // Turkish
            'Ş': 'S', 'İ': 'I', 'Ç': 'C', 'Ü': 'U', 'Ö': 'O', 'Ğ': 'G',
            'ş': 's', 'ı': 'i', 'ç': 'c', 'ü': 'u', 'ö': 'o', 'ğ': 'g',

            // Russian
            'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
            'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
            'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
            'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
            'Я': 'Ya',
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
            'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
            'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
            'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
            'я': 'ya',

            // Ukrainian
            'Є': 'Ye', 'І': 'I', 'Ї': 'Yi', 'Ґ': 'G',
            'є': 'ye', 'і': 'i', 'ї': 'yi', 'ґ': 'g',

            // Azerbaijanian
            'Ə': 'E',
            'ə': 'e',

            // Czech
            'Č': 'C', 'Ď': 'D', 'Ě': 'E', 'Ň': 'N', 'Ř': 'R', 'Š': 'S', 'Ť': 'T', 'Ů': 'U',
            'Ž': 'Z',
            'č': 'c', 'ď': 'd', 'ě': 'e', 'ň': 'n', 'ř': 'r', 'š': 's', 'ť': 't', 'ů': 'u',
            'ž': 'z',

            // Polish
            'Ą': 'A', 'Ć': 'C', 'Ę': 'e', 'Ł': 'L', 'Ń': 'N', 'Ó': 'o', 'Ś': 'S', 'Ź': 'Z',
            'Ż': 'Z',
            'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ó': 'o', 'ś': 's', 'ź': 'z',
            'ż': 'z',

            // Latvian
            'Ā': 'A', 'Č': 'C', 'Ē': 'E', 'Ģ': 'G', 'Ī': 'i', 'Ķ': 'k', 'Ļ': 'L', 'Ņ': 'N',
            'Š': 'S', 'Ū': 'u', 'Ž': 'Z',
            'ā': 'a', 'č': 'c', 'ē': 'e', 'ģ': 'g', 'ī': 'i', 'ķ': 'k', 'ļ': 'l', 'ņ': 'n',
            'š': 's', 'ū': 'u', 'ž': 'z'
        };

        // Make custom replacements
        for (var k in opt.replacements) {
            s = s.replace(RegExp(k, 'g'), opt.replacements[k]);
        }

        // Transliterate characters to ASCII
        if (opt.transliterate) {
            for (var k in char_map) {
                s = s.replace(RegExp(k, 'g'), char_map[k]);
            }
        }

        // Replace non-alphanumeric characters with our delimiter
        var alnum = (typeof(XRegExp) === 'undefined') ? RegExp('[^a-z0-9]+', 'ig') : XRegExp('[^\\p{L}\\p{N}]+', 'ig');
        s = s.replace(alnum, opt.delimiter);

        // Remove duplicate delimiters
        s = s.replace(RegExp('[' + opt.delimiter + ']{2,}', 'g'), opt.delimiter);

        // Truncate slug to max. characters
        s = s.substring(0, opt.limit);

        // Remove delimiter from ends
        s = s.replace(RegExp('(^' + opt.delimiter + '|' + opt.delimiter + '$)', 'g'), '');

        return opt.lowercase ? s.toLowerCase() : s;
    }

</script>
<script>
    var menu_parent_menu_id = $('#menu-parent_menu_id');
    var menu_position = $('#menu-position');
    var menu_icon_div = $('.field-menu-fa_icon');
    var menu_icon = $('#menu-fa_icon');
    var menu_image_div = $('.field-menu-image');
    var menu_image = $('#menu-image');
    menu_image_div.hide();
    menu_icon_div.hide();
    if (menu_position.val() == 'slider-under') {
        menu_icon_div.show();
        menu_image_div.hide();
    } else if (menu_position.val() == 'header' || menu_position.val() == 'other') {
        menu_icon_div.hide();
        menu_image_div.hide();
    }
    else if (menu_position.val() == '') {
    }
    else {
        menu_icon_div.hide();
        menu_image_div.show();
    }

    menu_position.change(function () {
//        alert( "Handler for .change() called." );
        if (menu_position.val() == 'slider-under') {
            menu_icon_div.show();
            menu_image_div.hide();
        } else if (menu_position.val() == 'header' || menu_position.val() == 'other') {
            menu_icon_div.hide();
            menu_image_div.hide();
        } else if (menu_position.val() == '') {

        }
        else {
            menu_icon_div.hide();
            menu_image_div.show();
        }
    });


    var menu_page_type = $('#menu-page_type');
    var lang_first_div = $('#lang-first-div');
    var lang_div_az = $('#lang-div-az');
    var lang_div_en = $('#lang-div-en');
    var lang_div_ru = $('#lang-div-ru');
    var menu_context_az = $('.field-menu-context_az');
    var menu_context_en = $('.field-menu-context_en');
    var menu_context_ru = $('.field-menu-context_ru');
    var menu_link = $('.field-menu-link');
    menu_link.hide();
    $(document).ready(function () {
        if (menu_page_type.val() == 'sp') {
//            alert('');
            lang_first_div.show();
            menu_context_az.show();
            menu_context_en.hide();
            menu_context_ru.hide();
//            lang_div_en.click(function() {
//                alert( "Handler for .click() called." );
//            });
        } else {
            lang_first_div.hide();
        }
        if (menu_page_type.val() == 'lp') {
            menu_link.show();
        } else {
            menu_link.hide();
        }
    });

    menu_page_type.change(function () {
//        alert( "Handler for .change() called." );
        if (menu_page_type.val() == 'lp') {
            menu_link.show();
//            lang_div_en.click(function() {
//                alert( "Handler for .click() called." );
//            });
        } else {
            menu_link.hide();
        }
        if (menu_page_type.val() == 'sp') {
            lang_first_div.show();
            menu_context_en.hide();
            menu_context_ru.hide();
//            lang_div_en.click(function() {
//                alert( "Handler for .click() called." );
//            });
        } else {
            lang_first_div.hide();
        }
    });
</script>

<script>
    var menu_name = $("#menu-name");
    var menu_slug_d = $("#menu-slug");
    menu_name.keyup(function () {
        str = menu_name.val();
        menu_slug_d.val(url_slug(str));
//        function string_to_slug() {
//            str = str.replace(/^\s+|\s+$/g, ''); // trim
//            str = str.toLowerCase();
//
//            // remove accents, swap ñ for n, etc
//            var from = "àáäâèéëêìíïîòóöôùúüûñçəğı·/_,:;";
//            var to   = "aaaaeeeeiiiioooouuuuncegi------";
//            for (var i = 0, l = from.length; i < l; i++) {
//                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
//            }
//
//            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
//                .replace(/\s+/g, '-') // collapse whitespace and replace by -
//                .replace(/-+/g, '-'); // collapse dashes

//            post_slug_d.val(str);
//        }
    });

</script>