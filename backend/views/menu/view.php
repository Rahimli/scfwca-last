<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Menu */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            ['attribute'=>'link',
                'value' => '/page/'.$model->slug,],
            ['attribute'=>'link',
                'value' => '/en/page/'.$model->slug,],
            ['attribute'=>'link',
                'value' => '/ru/page/'.$model->slug,],
            'parent_menu_id',
            'name',
            'name_az',
            'name_en',
            'name_ru',
            'order_index',
            'page_type',
            'position',
            'active',
            'context_az:ntext',
            'context_en:ntext',
            'context_ru:ntext',
            'slug',
            'date',
            'fa_icon',
            'image',
            'link',
        ],
    ]) ?>

</div>
