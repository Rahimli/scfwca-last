<?php

namespace backend\controllers;

use Yii;
use backend\models\Appeal;
use backend\models\AppealSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AppealController implements the CRUD actions for Appeal model.
 */
    class AppealController extends Controller
    {
        /**
         * @inheritdoc
         */
        public function behaviors()
        {
            return [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ];
        }

        /**
         * Lists all Appeal models.
         * @return mixed
         */
        public function actionIndex()
        {
            if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $data_years = array();
            $time = new \DateTime('now');
            $now_y = $time->format('Y');
            $now_y = (int)$now_y;
            $i = 0;
            for ($x = 0; $x <= 6; $x++) {
                $i += 1;
                $appeals = Appeal::find()->where(['between', 'date', ($now_y) . '-00-00', ($now_y) . '-12-31']);
                $data_years['year_' . $now_y] = array('year' => $now_y, 'count' => $appeals->count());
                $now_y -= 1;
            }
            $searchModel = new AppealSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'data_years' => $data_years,
            ]);
            }else{
                if(!Yii::$app->user->isGuest){
                    Yii::$app->user->logout();
                }
                return $this->goHome();
            }
        }

        /**
         * Displays a single Appeal model.
         * @param integer $id
         * @return mixed
         */
        public function actionView($id)
        {
            if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
            }else{
                if(!Yii::$app->user->isGuest){
                    Yii::$app->user->logout();
                }
                return $this->goHome();
            }
        }

        /**
         * Creates a new Appeal model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
        public function actionCreate()
        {
            if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $model = new Appeal();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            }else{
                if(!Yii::$app->user->isGuest){
                    Yii::$app->user->logout();
                }
                return $this->goHome();
            }
        }

        /**
         * Updates an existing Appeal model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         */
        public function actionUpdate($id)
        {
            if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            }else{
                if(!Yii::$app->user->isGuest){
                    Yii::$app->user->logout();
                }
                return $this->goHome();
            }
        }

        /**
         * Deletes an existing Appeal model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         */
        public function actionDelete($id)
        {
            if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
            }else{
                if(!Yii::$app->user->isGuest){
                    Yii::$app->user->logout();
                }
                return $this->goHome();
            }
        }

        /**
         * Finds the Appeal model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return Appeal the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
            if (($model = Appeal::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
