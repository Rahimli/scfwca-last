<?php
namespace backend\controllers;

use backend\models\Appeal;
use backend\models\Menu;
use backend\models\Post;
use backend\models\PostView;
use backend\models\Question;
use backend\models\Slider;
use Yii;
use yii\db\Expression;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */

    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')){
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        #$programs = Programs::find()->where(['>=', 'close_date', $today])->all();

        $questions_count = Question::find()->orderBy(['date' => SORT_DESC])->count();
        $menus_count = Menu::find()->orderBy(['order_index' => SORT_ASC])->count();
        $posts_count = Post::find()->orderBy(['date' => SORT_DESC])->count();
        $slider_images_count = Slider::find()->orderBy(['order_index' => SORT_ASC])->count();
        $appeals_count = Appeal::find()->orderBy(['date' => SORT_DESC])->count();
//        $appeals = Appeal::find()->orderBy(['date'=>SORT_DESC])->count();
        $data_post = array();
        $data_years = array();
        $data_post['last_24'] = PostView::find()->andWhere(['>=', 'date', new Expression('UNIX_TIMESTAMP(NOW() - INTERVAL 1 DAY)')])->count();
        $data_post['last_7'] = PostView::find()->where(['>=', 'date', new Expression('UNIX_TIMESTAMP(NOW() - INTERVAL 7 DAY)')])->count();
        $data_post['last_30'] = PostView::find()->where(['>=', 'date', new Expression('UNIX_TIMESTAMP(NOW() - INTERVAL 30 DAY)')])->count();
        $data_post['last_365'] = PostView::find()->where(['>=', 'date', new Expression('UNIX_TIMESTAMP(NOW() - INTERVAL 365 DAY)')])->count();

        $now_y = $time->format('Y');
        $now_y = (int)$now_y;
        $i = 0;
        for ($x = 0; $x <= 6; $x++) {
            $i+=1;
            $result = $time->format($now_y - $x.'-00-00 00:00:00');
            $startDateSearch = new \DateTime(($now_y).'-00-00');
            $endDateSearch   = new \DateTime(($now_y + $x).'-00-00');
            $appeals = Appeal::find()->where(['between', 'date', ($now_y).'-00-00', ($now_y).'-12-31' ]);
//            die($appeals->count());
            $data_years['year_'.$now_y] = array('year'=>$now_y,'count'=>$appeals->count());
            $now_y -= 1;
//            var_dump($orders->count());
        }
//        die($data_years['year_2015']['year']);
//        $data_post['last_365'] = Appeal::find()->where(['date'=>2017])->count();
//        $date = new \DateTime('2000-01-01');
//        $result = $time->format($now_y.'-00-00 00:00:00');
//        $result = intval ( date('Y'));
//        die($result);

//        die(date('Y-m-d H:i:s'));


        $context = [
            'data_post' => $data_post,
            'questions_count' => $questions_count,
            'menus_count' => $menus_count,
            'posts_count' => $posts_count,
            'slider_images_count' => $slider_images_count,
            'appeals_count' => $appeals_count,
            'data_years' => $data_years,
        ];
        return $this->render('index', $context);
        }else{
            return $this->render('login');
        }
    }



    /**
     * Login action.
     *
     * @return string
     */

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}