<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use backend\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1') && (Yii::$app->user->identity->is_superuser == '1')){
            $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        }else{
            if(!Yii::$app->user->isGuest){
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */

    public function actionView($id){

        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1') && (Yii::$app->user->identity->is_superuser == '1')){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
        }else{
            if(!Yii::$app->user->isGuest){
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function passwordTester(){

    }
    public function actionCreate()
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1') && (Yii::$app->user->identity->is_superuser == '1')){
        $model = new User();

        if ($model->load(Yii::$app->request->post())  ) {
//            die($model->setPasswordr($model->password));
            if(isset($model->password) && !empty($model->password) && (strlen($model->password) >= 8)) {
                $model->password_hash = $model->setPasswordr($model->password);
                $model->password = '';
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }else{
                $model->addError($model->password);
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        }else{
            if(!Yii::$app->user->isGuest){
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1') && (Yii::$app->user->identity->is_superuser == '1')){
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if(trim($model->password)==''){
                $model->password = '';
//                $model->save();
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }

            }else{
                if((strlen($model->password) >= 8)){
                    $model->password_hash = $model->setPasswordr($model->password);
                    $model->password = '';
                    if($model->save()){
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }else{

                }

            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
        }else{
            if(!Yii::$app->user->isGuest){
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1') && (Yii::$app->user->identity->is_superuser == '1')){
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
        }else{
            if(!Yii::$app->user->isGuest){
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
