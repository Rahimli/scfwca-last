<?php

namespace backend\controllers;

use backend\models\PostImage;
use Yii;
use backend\models\Post;
use backend\models\PostSearch;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $searchModel = new PostSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            if (!Yii::$app->user->isGuest) {
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            if (!Yii::$app->user->isGuest) {
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {

        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
//                $fileName = 'file';
//                $uploadPath = 'post-slider';
//                $file_urls = array();
//                if (isset($_FILES[$fileName])) {
//                    $file = \yii\web\UploadedFile::getInstanceByName($fileName);
//
//                    //Print file data
////                    print_r($file);
////                    die(var_dump($file_urls));
//                    if ($file->saveAs($uploadPath . '/' . $file->name)) {
//                        array_push($file_urls,$uploadPath . '/' . $file->name);
//                        //Now save file data to database
//
//                        echo \yii\helpers\Json::encode($file);
//                    }
//                }
//            return false;

            $model = new Post();
            $web_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

            $file_destination = $model->image;
            if (!empty($file_destination)) {
                    // echo $model->image;
//                            die(var_dump(parse_url($url, PHP_URL_HOST)));


                    $md5_val = md5(uniqid(rand(), true));
                    $md5_val_2 = md5(uniqid(rand(), true));
//                            $file_destination = '/media/news/' . $md5_val . '-' . $md5_val_2 . '-normal' . '.png';
                    $modal_url = "";
                    $explode = explode("/", $model->image);
                    if (!strpos($explode[3], ".")) {
                        $modal_url = explode("/", $model->image)[3] . '/';
                    }
                    $file_destination_png = '/media/news/' . $modal_url . $md5_val . '-' . $md5_val_2 . '.png';
                }




            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
                if (!empty($model->imageFiles)) {
                    if ($model->upload()) {
                        foreach ($model->imageFiles as $file) {
                            $post_image_model = new PostImage();
                            $md5_val = md5(uniqid(rand(), true));
                            $file->saveAs(Url::to('@frontend/web/post-image/') . $model->id.'-post-image'.$md5_val.$file->baseName . '.' . $file->extension);
                            $post_image_model->image_url = '/post-image/' . $model->id.'-post-image'.$md5_val.$file->baseName . '.' . $file->extension;
                            $post_image_model->post_id = $model->id;
                            $post_image_model->save();
                        }
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            if (!Yii::$app->user->isGuest) {
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
                if (!empty($model->imageFiles)) {
                    if ($model->upload()) {
                        foreach ($model->imageFiles as $file) {
                            $post_image_model = new PostImage();
                            $md5_val = md5(uniqid(rand(), true));
                            $file->saveAs(Url::to('@frontend/web/post-image/') . $model->id.'-post-image'.$md5_val.$file->baseName . '.' . $file->extension);
                            $post_image_model->image_url = '/post-image/' . $model->id.'-post-image'.$md5_val.$file->baseName . '.' . $file->extension;
                            $post_image_model->post_id = $model->id;
                            $post_image_model->save();
                        }
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                $model->menu_ids = ArrayHelper::map($model->menus, 'name', 'id');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            if (!Yii::$app->user->isGuest) {
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            if (!Yii::$app->user->isGuest) {
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }
    public function actionDeleteimages($id)
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
            $message_code = 0;
            $model = $this->findModel($id);
            if (Yii::$app->request->isGet && Yii::$app->request->isAjax) {
                try{
                $image_id = Yii::$app->request->get('image_id');
                $model_post_image = PostImage::find()->where(['id'=>$image_id,'post_id'=>$id])->one();
                $model_post_image->delete();
                $message_code = 1;
                }catch (Exception $e){
                    $message_code = $image_id;
                }

                $data = array('message_code' => $message_code);
                \Yii::$app->response->format = 'json';
                return $data;
            }else{
                if (!Yii::$app->user->isGuest) {
                    Yii::$app->user->logout();
                }
                return $this->goHome();
            }
        } else {
            if (!Yii::$app->user->isGuest) {
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
    }

    public function actionUpload()
    {
        $fileName = 'file';
        $uploadPath = 'post-slider';
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->is_staff == '1')) {
        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);

            //Print file data
            //print_r($file);
//                die('asas');
            if ($file->saveAs($uploadPath . '/' . $file->name)) {
                //Now save file data to database

                echo \yii\helpers\Json::encode($file);
            }
        } else {
            return $this->render('upload');
        }
        } else {
            if (!Yii::$app->user->isGuest) {
                Yii::$app->user->logout();
            }
            return $this->goHome();
        }
        return false;
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}


